# 如何進入遊戲

玩家至少需要購買5個英雄NFT，然後將英雄NFT從“錢包資產”轉到”“遊戲資產”後，才能進入遊戲進行PVP或PVE冒險。具體的操作步驟如下：

第一步：進入官網（網址：http://beta.toril.io/ ）。點擊“登錄”連接你的錢包。

![](../.gitbook/assets/3.1.png)

![](../.gitbook/assets/3.2.png)

第二步：點擊“市場”，在英雄商城選擇5個心儀的英雄NFT進行購買.

![](../.gitbook/assets/3.3.png)

第三步：購買成功後，點擊“資產”，在”錢包資產“欄中選擇已購買英雄的”加入遊戲“按鈕，在彈出的頁面中點擊“確認”按鈕。

![](../.gitbook/assets/3.4.png)

![](../.gitbook/assets/3.5.png)

第四步：點擊“遊戲資產”，可看到英雄NFT已成功轉入（需將5個英雄NFT都轉入遊戲資產中）。

![](../.gitbook/assets/3.6.png)

第五步：點擊導航欄的“下載”按鈕下載遊戲的APK包，安裝完成後即可進入遊戲。

![](../.gitbook/assets/3.7.png)

