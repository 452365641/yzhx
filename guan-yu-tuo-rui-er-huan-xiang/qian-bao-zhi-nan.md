# 錢包指南

托瑞爾幻想是一個基於BSC鏈的項目，所以玩家需要創建一個BSC地址，來安裝一個 MetaMask 錢包。

MetaMask 是在谷歌瀏覽器上使用的插件式以太坊錢包。您只需要在谷歌瀏覽器中添加相應的擴展程序，更方便。

第一步：下載並安裝MetaMask（網址：https://metamask.io/download.html）。安裝完成後瀏覽器右上角會出現狐狸標誌。只需單擊一下，您就可以導入您的錢包或創建一個新錢包。

![](../.gitbook/assets/1.png)

第二步：添加BSC鏈，由於 MetaMask 默認沒有列出 BSC 鏈，您需要手動添加。點擊 MetaMask 界面頂部的區塊鍊網絡，然後選擇自定義 RPC。

![](../.gitbook/assets/2.png)

將以下信息一一填寫，點擊保存：

網絡：Binance Smartchain

網址：https://bsc-dataseed.binance.org/

編號：56

符號：BNB

網址：https://www.bscscan.com/

稍後，您可以看到 Binance Smartchain 有一個額外的選項。您可以將現有的 BSC 帳戶私人密鑰導入 MetaMask。
